# Rust Belt Rust 2018 website

## What is it

This is the web site for Rust Belt Rust 2018, deployed at [conf2018.rust-belt-rust.com](conf2018.rust-belt-rust.com). It is information for attendees, and also a learning project for the builders! See some notes about that below.

## How to run it

First time:
* Pull down a local copy
* `brew install zola`

Every time:
* `zola serve`

## Goals and framework

I have a few goals with building this web site.

- Separate content from rendering
  - ideally with content in a simple form, such as Markdown
- Few dependencies
  - ideally something built in Rust

Looking around, I found [Zola](https://www.getzola.io/), which sounds right for me. Zola has powerful support for blogs, but I'm not building a blog. Zola claims first class support for non-blog sites and Zola's own web site and documentation are non-blog sites built with Zola.

### My struggles with Zola

I'll start by saying I have little experience building web sites. I was hoping to use existing tools and scaffolding to pull something together without needing deep understanding.

#### Themes

Most published themes for Zola are blog oriented. I tried starting with a theme, [even](https://github.com/Keats/even), grabbing its content as a staring point and overriding things, but quickly found myself getting errors that I didn't understand.

Once I realized that starting with the _even_ theme and copying a template from the theme up into my site would trigger errors, I decided that I was in over my head on using the theme. I just wanted to try modifying the front page away from being a blog.

To be precise:

`cp themes/even/templates/index.html templates`

triggered the error below:

```shell
$ zola serve
Building site...
-> Creating 3 pages (0 orphan) and 1 sections
Done in 91ms.

Listening for changes in /Users/ba/gitlab/experiment/{content, templates, config.toml, static, sass}
Web server is available at http://127.0.0.1:1111
Press Ctrl+C to stop

Change detected @ 2018-06-12 05:45:37
-> Template changed /Users/ba/gitlab/experiment/templates/index.html
Failed to build the site
Error: Failed to render pager 1 of section '/Users/ba/gitlab/experiment/content/_index.md'
Reason: Template 'post_macros.html' not found
Done in 5ms.
```

### Front page

Zola's handling of the front page still confuses me. My front page is just a web page. It has content. I want that content to be Markdown, just like all of the other pages. Stumbling around more than a little, I found that this "works":

    {% for page in section.pages -%}
      {{page.content | safe}}
    {%- endfor %}

I had a markdown file named `index.md`, but once I had this code which picks up all top level Markdown files, I figured I'd avoid the implication of a magic name, and now call it `front.md`.

The confusing bit here is `zola serve` does not correctly update the front page when I change `front.md`. It does report seeing a change:

    Change detected @ 2018-06-12 05:50:59
    -> Content changed /Users/ba/gitlab/rust-belt-rust/content/front.md
    Done in 2ms.

But the page in the browser does not change, even with a hard reload in the browser. I have to stop and restart `zola serve` to see the change.

Markdown updates for other pages, such as `pages/conduct.md`, update correctly, and completely automatically, in the browser.
