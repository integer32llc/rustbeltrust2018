+++
+++

# Rust Belt Rust 2018, Ann Arbor, October 19 & 20

![Rust Belt Rust, smoke to code](/logo.jpg "Rust Belt Rust")

Rust Belt Rust 2018 will take place on Friday October 19 & Saturday October 20 at [Wyndham Garden, Ann Arbor, Michigan](https://www.wyndhamhotels.com/wyndham-garden/ann-arbor-michigan/wyndham-garden-ann-arbor/overview)

## Sessions

<a class="call-to-action" href="/sessions">See the talks</a>

We have an exciting lineup of workshops and talks for you to learn about many different aspects of
Rust! There will also be a session of lightning talks where YOU can contribute!

## Tickets

<a class="call-to-action" href="https://www.eventbrite.com/e/rust-belt-rust-conference-2018-registration-47481792319">Buy Tickets</a>

Tickets are $200; $75 for students. The first day of the conference is longer format, hands-on
workshops, and the second day is shorter presentations and lighting talks. One of the workshop
options on the first day is RustBridge. RustBridge is for people who are underrepresented in tech
and is free (a $20 deposit holds your spot and will be refunded to you when you arrive). If you are
attending RustBridge as well as day 2, please choose a Day 2 general admission or student ticket as
appropriate.

## Diversity Scholarships

<a class="call-to-action" href="https://goo.gl/forms/UnjmzOgICvVBN4Fp2">Apply now!</a>

Diversity scholarships including ticket price, flight, and lodging are available for people who are
underrepresented in tech. The application is open until 5pm ET Friday Sept 14, 2018, and
scholarship recipients will be notified by Sept 21, 2018.

## Sponsors

Thank you to our wonderful [sponsors](/sponsors); this conference wouldn't be possible without their support!

## Lodging

We have reduced hotel rate at the Wyndham Garden Ann Arbor of $89 per night. Please book your room before 2018/9/19, when room discount expires. Go to [Wyndham RBR Lodging](https://www.wyndhamhotels.com/groups/gn/rust-belt-rust-conference). It should be possible to
use the block code **RBR** when making a reservation or simply say you are with the *Rust Belt Rust* block. The hotel seems to be struggling with the group code and they recommend using the link above or calling the Ann Arbor location directly at +1-734-665-4444.

## Accessibility

Our goal is to make the conference, workshops and all other related events accessible to people with disabilities or needing accommodations. However, we are aware that accessibility issues are diverse and we may not have everything covered in our plans. Please reach out! We will do our very best to confirm that we have what you need in place. Email us at [organizers@rust-belt-rust.com](mailto:organizers@rust-belt-rust.com)

## Childcare

If you will need childcare, please email [organizers@rust-belt-rust.com](mailto:organizers@rust-belt-rust.com) as soon as possible. We will work with you to find a licensed childcare provider at your hotel and will provide a stipend to cover the cost.

## Slack

We have a [Slack instance](https://rust-belt-rust-slack.herokuapp.com/) that you are welcome to join. Potential topics include things to do around Ann Arbor before, during, and after the conference; your Rust projects; ideas for the unconference; or just arranging meals!

## Conduct

Rust Belt Rust is dedicated to providing a harassment-free conference experience for everyone. Please see our [code of conduct](/conduct) for more details.
