+++
title = "Sponsors"
path = "sponsors"
+++
# Rust Belt Rust Conference Sponsors

We love our sponsors!

Please take a look at our [sponsorship prospectus](rust-belt-rust-2018-sponsorship-prospectus.pdf) if you're interested in joining this list.

## Steel

Your logo could be here!

## Gold

[![Parity Technologies](parity.png)](https://paritytech.io)

[![Mozilla](mozilla.png)](https://www.mozilla.org/)

## Silver

[![Heal Pay](healpay.png)](https://healpay.com/)
[![Distil](distil.png)](https://www.distilnetworks.com/)

## Bronze

Your logo could be here!

## Speaker sponsors

These sponsors have generously helped our speakers get to the conference:

[![Mozilla](mozilla.png)](https://www.mozilla.org/)

[![PingCAP](01-PingCAP-标准标识-横向组合.svg)](https://www.pingcap.com/)

## Infrastructure sponsors

These sponsors have provided people-hours and infrastructure to make the conference happen:

[![Integer 32](integer32.png)](https://integer32.com/)

## Prize sponsors

These sponsors have provided raffle prizes and discounts!

[![Manning Publications](manning.png)](https://www.manning.com/)

## Individual sponsors

These generous individuals are supporting the conference with additional donations:

* Justin Geibel
* Ashley Williams
* Nate Sutton
* Donovan Preston
* Jason Foreman
* Rob Tsuk
* Franco Sasieta
* Douglas Balog
* Alex Crichton
